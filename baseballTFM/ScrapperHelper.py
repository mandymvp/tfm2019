import requests
from bs4 import BeautifulSoup



def get_columns_names(id_table ,url):
    """
    Scrapping function to get table heads (Name of the statistics)
    """
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')

    table_heads = soup.find("table", {
        "id": id_table
    }).thead.tr.findAll("th")
    heads = []

    for thead in table_heads:
        heads.append(thead.text)
    return heads

def get_table_stats(id_table ,url):
    """
    Scrapping function to get table body. All time stats for a player
    """
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    historic_stats = soup.find("table", {
        "id": id_table
    }).tbody.findAll("tr")

    table_stats = []

    for year in historic_stats:
        stats_year = []
        line_year = year.findAll(["th", "td"])
        for stats in line_year:
            stats_year.append(stats.text)
        table_stats.append(stats_year)
    return table_stats

def get_table_stats_ext(id_table, url):
    """
    Scrapping function to get table body. With extended attributes
    """
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    historic_stats = soup.find("table", {
        "id": id_table
    }).tbody.findAll("tr")

    table_stats = []
    for year in historic_stats:
        stats_year = []
        line_year = year.findAll(["th", "td"])
        line_player_id = year.find("td" ,{"class" :"left"})['data-append-csv']
        line_player_href = year.find("td").a['href']
        for stats in line_year:
            stats_year.append(stats.text)
        stats_year.extend([line_player_id ,line_player_href])
        table_stats.append(stats_year)
    return table_stats