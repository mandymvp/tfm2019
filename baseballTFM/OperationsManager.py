import pandas as pd
from tqdm import tqdm

from Environment import *
from OperationsHelper import *

league_stats = pd.read_csv(leagues_stats_file)


def sum_multiple_league_stats(leagues, year, leagues_id):
    """
    Sum every league in which a player participate
    :param leagues: Leagues where a player played
    :param year: play year
    :param leagues_id: League identifier. Combination of ASCII value for letters in "league abbreviation" and number
     of leagues played. Ex: 2_325 means played in 2 leagues AA and AAA
    :return:
    """
    global league_stats
    leagues_dataframe = pd.DataFrame()
    for league in leagues:
        player_league_stats = league_stats[
            (pd.to_numeric(league_stats['Year']) == year) & (league_stats['Lev'] == league)]
        if player_league_stats.empty:
            print("ERROR FINDING YEAR {0} AND LEAGUE {1} ".format(year, league))
            break
        leagues_dataframe = leagues_dataframe.append(player_league_stats, ignore_index=True, sort=False)
    level_stats = []
    for stat in used_stats:
        level_stats.append(leagues_dataframe[stat].astype(int).sum())
    sum_stats_dataframe = pd.DataFrame([level_stats], columns=used_stats)
    sum_stats_dataframe['Leagues_id'] = leagues_id

    return sum_stats_dataframe


def minor_leagues_totals_by_players():
    """
    Sum statistics for every player minor league appearance.
    Ex: player1 played in [AA,AAA]. We take totals for both leagues. (Eventually has to be averaged)
    :return:
    """
    if league_stats.empty:
        print("ERROR")

    before_debuts_stats = pd.read_csv(before_debut_stats_file)
    league_stats_id = {}

    # Final dataframe with league totals corresponding to every player season before debut.
    milb_totals_for_players = pd.DataFrame(before_debuts_stats[basic_info])
    league_totals_stats = pd.DataFrame(columns=used_stats)

    for index, player in tqdm(before_debuts_stats.iterrows(), desc='Calculating MiLB totals', total=len(before_debuts_stats)):

        player_league_combination = player['Lg'].split(',')
        combination_len = len(player_league_combination)
        combination_value = get_combination_value(player_league_combination)
        year = player['Year']
        leagues_id = str(combination_len) + '_' + str(combination_value)
        # Combination to identify a league in a year in order to save extra calculations.
        #  A player who played in same leagues and same year will use data existing in this id, if already save.
        combination_id = str(year) + '_' + str(combination_len) + '_' + str(combination_value)

        if combination_id in league_stats_id.keys():
            league_totals_stats = league_totals_stats.append(league_stats_id[combination_id],
                                                                                       sort=False, ignore_index=True)
        else:
            current_league_sum = sum_multiple_league_stats(player_league_combination, year, leagues_id)
            league_totals_stats = league_totals_stats.append(current_league_sum, sort=False)
            league_stats_id[combination_id] = current_league_sum

    league_totals_stats = league_totals_stats.reset_index()

    milb_totals_for_players = pd.concat([milb_totals_for_players, league_totals_stats], axis=1)
    milb_totals_for_players[output_fields].to_csv(milb_totals_by_players_file)



def mayor_leagues_totals_by_players():
    """
    Sum statistics for MLB appearances
    :return:
    """
    debuts_stats = pd.read_csv(debut_stats_file)

    mlb_stats_for_player_dataframe = pd.DataFrame()
    players_mlb_league_stats = pd.DataFrame(debuts_stats[['Name', 'Age', 'Player_id', 'Lg', 'B']])

    for index, player in tqdm(debuts_stats.iterrows(), desc='Calculating MLB totals', total=len(debuts_stats)):
        player_mlb_stats = league_stats[
            (pd.to_numeric(league_stats['Year']) == player['Year']) & (league_stats['Lev'] == 'MLB')]
        if player_mlb_stats.empty:
            print("ERROR FINDING YEAR {0} AND LEAGUE {1} ".format(league_stats['Year'], league_stats['Lev']))
            break
        mlb_stats_for_player_dataframe = mlb_stats_for_player_dataframe.append(player_mlb_stats, ignore_index=True,
                                                                               sort=False)

    players_mlb_league_stats = pd.concat([players_mlb_league_stats, mlb_stats_for_player_dataframe], axis=1)
    players_mlb_league_stats['Leagues_id'] = '1_'+str(get_combination_value(['MLB']))
    players_mlb_league_stats[output_fields].to_csv(mlb_totals_stats_by_player_file)


def average_values():
    """

    :return:
    """
    # Load data
    milb_totals_for_players = pd.read_csv(milb_totals_by_players_file)
    mlb_totals_for_players = pd.read_csv(mlb_totals_stats_by_player_file)
    before_debut_players_stats = pd.read_csv(before_debut_stats_file)
    debut_players_stats = pd.read_csv(debut_stats_file)

    # Add ids on dataframes that needs it
    before_debut_players_stats = pd.concat([before_debut_players_stats, milb_totals_for_players['Leagues_id']], axis=1)
    debut_players_stats = pd.concat([debut_players_stats, mlb_totals_for_players['Leagues_id']], axis=1)

    # Calculate means
    mean_stats_calculator(milb_totals_for_players, by_stat, stats_to_average)[output_fields].to_csv(milb_league_ave)
    mean_stats_calculator(mlb_totals_for_players, by_stat, stats_to_average)[output_fields].to_csv(mlb_league_ave)
    mean_stats_calculator(before_debut_players_stats, by_stat, stats_to_average)[output_fields].to_csv(before_debut_players_ave)
    mean_stats_calculator(debut_players_stats, by_stat, stats_to_average)[output_fields].to_csv(debut_players_ave)



def values_over_league_average():

    milb_ave = pd.read_csv(milb_league_ave)
    mlb_ave = pd.read_csv(mlb_league_ave)
    before_debut_ave = pd.read_csv(before_debut_players_ave)
    debut_ave = pd.read_csv(debut_players_ave)

    info_milb_players = pd.DataFrame(milb_ave[basic_info+['Leagues_id']])
    info_mlb_players = pd.DataFrame(mlb_ave[basic_info+['Leagues_id']])

    for stat in tqdm(stats_to_average, desc='Calculating players differences over league average'):
        info_milb_players[stat] = before_debut_ave[stat] - milb_ave[stat]
        info_mlb_players[stat] = debut_ave[stat] - mlb_ave[stat]

    info_milb_players.to_csv(milb_players_values_over_league_average)
    info_mlb_players.to_csv(mlb_players_values_over_league_average)


values_over_league_average()
