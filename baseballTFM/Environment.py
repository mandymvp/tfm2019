# Globals
import os

# Last year to start get players data
last_debut_year = 2017
# Number of years to backward and get data
amount_of_years = 3

basic_info = ['Name','Age','Player_id','Lg','B','Year']
stats_to_scrape = ['G', 'PA', 'AB', 'R', 'H', '2B', '3B', 'HR', 'RBI', 'SB', 'BB', 'SO', 'HBP', 'SF', 'SH']
used_stats = ['G', 'PA', 'AB', 'R', 'H', '2B', '3B', 'HR', 'RBI', 'SB', 'BB', 'SO', 'S', 'HBP', 'SF', 'SH']
stats_to_average = ['AB', 'R', 'H', '2B', '3B', 'HR', 'RBI', 'SB', 'BB', 'SO', 'S', 'HBP', 'SF', 'SH']
totals_columns = used_stats + ['Leagues_id']


# ======================================
# Debut data scrape constants
debuts_page = 'https://www.baseball-reference.com/leagues/MLB/{0}-debuts.shtml'
debuts_id_table = 'misc_bio'
debuts_columns = ['Name', 'Age', 'Player_id', 'B', 'Page_url', 'Year']
debuts_info_file = 'data/debuts/debuts_info.csv'
# ======================================

# ======================================
# League data scrape constants
league_stats_page = 'https://www.baseball-reference.com/register/league.cgi?year={0}'
league_table_id = 'league_batting'
league_columns_selected = ['Lev'] + stats_to_scrape
# Levels are parsed to unify them with rest of the data
levels = {
    'Major League': 'MLB',
    'AAA': 'AAA',
    'AA': 'AA',
    'Adv A': 'A+',
    'A': 'A',
    'Short-Season A': 'A-',
    'Rookie': 'Rk',
    'Fall': 'F',
    'Winter': 'W',
    'Foreign Rookie': 'FRk',
    'Independent': 'I'
}
leagues_stats_file = 'data/leagues/leagues_stats_data.csv'

# ======================================

# ======================================
# Players stats data scrape constants
home_page = 'https://www.baseball-reference.com'
players_stats_table_id = 'batting_standard'
players_stats_output_columns = ['Name', 'Age', 'Player_id', 'Lg', 'Tm', 'B', 'Year', 'DYear'] + used_stats

before_debut_stats_file = 'data/players_stats/milb_before_debut_stats.csv'
debut_stats_file = 'data/players_stats/mlb_debut_stats.csv'

# ======================================

# ======================================
# Totals calculations constants
totals_location = 'data/totals'
output_fields = basic_info + totals_columns
milb_totals_by_players_file = os.path.join(totals_location, 'milb_league_stats_for_player.csv')
mlb_totals_stats_by_player_file = 'data/totals/mlb_league_stats_for_player.csv'

# ======================================

# ======================================
# Average calculations constants
average_location = 'data/averaged'
by_stat = 'PA'
before_debut_players_ave = os.path.join(average_location, 'before_debut_players_ave.csv')
debut_players_ave = os.path.join(average_location, 'debut_players_ave.csv')
milb_league_ave = os.path.join(average_location, 'milb_league_ave.csv')
mlb_league_ave = os.path.join(average_location, 'mlb_league_ave.csv')

average_files = {
    milb_totals_by_players_file: milb_league_ave,
    mlb_totals_stats_by_player_file: mlb_league_ave,
    before_debut_stats_file: before_debut_players_ave,
    debut_stats_file: debut_players_ave
}

# ======================================


# ======================================
# Players values over league average
players_values = 'data/players_diff_values'
milb_players_values_over_league_average = os.path.join(players_values, 'milb_players_values_over_league_average.csv')
mlb_players_values_over_league_average = os.path.join(players_values, 'mlb_players_values_over_league_average.csv')

# ======================================

