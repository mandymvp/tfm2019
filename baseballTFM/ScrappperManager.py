import pandas as pd
from ScrapperHelper import *
from Environment import *
from OperationsHelper import *
from tqdm import tqdm
def scrape_debuts():
    """
    Get and export to .csv scrapped data from players's debut
    :return:
    """
    columns_name = get_columns_names(debuts_id_table, debuts_page.format(last_debut_year))
    columns_name.extend(("Player_id", "Page_url"))
    debuts_dataframe = pd.DataFrame(columns=columns_name)

    for i in tqdm(range(amount_of_years),desc='Scrapping players debut information'):
        current_year = last_debut_year - i
        current_debut_page = debuts_page.format(current_year)
        current_year_info = pd.DataFrame(get_table_stats_ext(debuts_id_table, current_debut_page), columns=columns_name)
        current_year_info['Year'] = current_year

        debuts_dataframe = debuts_dataframe.append(current_year_info, 0, sort=False)
    # Avoiding scrape Pitchers. 1 is the number for pitchers
    debuts_dataframe = debuts_dataframe[~debuts_dataframe["Pos"].str.contains('1')]
    debuts_dataframe = debuts_dataframe.reset_index()

    debuts_dataframe[debuts_columns].to_csv(debuts_info_file)


def get_league_totals_stats(table_stats):
    """
    Sum every league stats belonging to same level.
    Ex: Level AAA has three leagues. This leagues will be sum as one representing Level AAA as League AAA
    :param table_stats:
    :return:
    """
    year_leagues_summary = []
    for level, level_parse in levels.items():
        # Replace hard spaces
        level_to_compare = level.replace(" ", "\xa0")
        league_level_stats = table_stats.loc[table_stats['Lev'] == level_to_compare]
        level_stats = [level_parse]
        for stat in stats_to_scrape:
            level_stats.append(league_level_stats[stat].astype(int).sum())
        year_leagues_summary.append(level_stats)

    return pd.DataFrame(year_leagues_summary, columns=league_columns_selected)


def get_leagues_stats():
    """
    Get and export to .csv scrapped data from every league

    :return:
    """
    # Get columns names
    columns_names = get_columns_names(league_table_id, league_stats_page.format(last_debut_year))

    # Create final dataframe
    league_stats_dataframe = pd.DataFrame(columns=league_columns_selected)

    # Get data
    for i in tqdm(range(amount_of_years + 1),desc='Scrapping leagues statistics'):
        current_year = last_debut_year - i
        current_year_stats = pd.DataFrame(get_table_stats(league_table_id, league_stats_page.format(current_year)),
                                          columns=columns_names)
        current_year_sum_stats = get_league_totals_stats(current_year_stats)
        current_year_sum_stats['Year'] = int(current_year)

        league_stats_dataframe = league_stats_dataframe.append(
            current_year_sum_stats, 0, sort=False)

    # Add singles to dataframe
    league_stats_dataframe = getSinglesStat(league_stats_dataframe)

    league_stats_dataframe.to_csv(leagues_stats_file)


def get_players_stats():
    players_debuts = pd.read_csv(debuts_info_file)
    if players_debuts.empty:
        print("Error")
    # Scrapped Player stats name.
    player_stats_names = get_columns_names(players_stats_table_id, home_page + players_debuts['Page_url'][0])
    # New dataframes for all players and his stats
    filter_debuts_players_data = pd.DataFrame(columns=players_debuts.columns.values)
    complete_milb_players_stats = pd.DataFrame(columns=player_stats_names)
    complete_mlb_players_stats = pd.DataFrame(columns=player_stats_names)

    for index, player in tqdm(players_debuts.iterrows(), desc='Scrapping players statistics',total=len(players_debuts)):

        # Scrapped Stats of current "player"
        player_stats = get_table_stats(players_stats_table_id, home_page + player['Page_url'])
        player_dataframe = pd.DataFrame(player_stats, columns=player_stats_names)

        # Find data before player´s  year debut
        year_before_debut = player_dataframe[pd.to_numeric(player_dataframe['Year']) == (player['Year'] - 1)]

        # If no data in year before player´s debut, skip process
        if year_before_debut.empty:
            continue

        # Find data on plyer´s year debut
        debut_year = player_dataframe[(pd.to_numeric(player_dataframe['Year']) == (player['Year'])) & (
                    (player_dataframe['Lg'] == 'NL') | (player_dataframe['Lg'] == 'AL') | (
                        player_dataframe['Lg'] == 'MLB'))]

        # If no data in year player´s debut, skip process
        if debut_year.empty:
            continue

        # Check for players who play for more than one team in his debut year. Take totals for that year
        if len(debut_year) > 1:
            debut_year = debut_year[debut_year['Tm'] == 'TOT']

        # Append player´s debut info
        filter_debuts_players_data = filter_debuts_players_data.append(players_debuts.iloc[[index]], ignore_index=True)

        # Append before debut player statistics
        complete_milb_players_stats = complete_milb_players_stats.append(year_before_debut, ignore_index=True)

        # Append debut players statistics
        complete_mlb_players_stats = complete_mlb_players_stats.append(debut_year, ignore_index=True)

        # Change variable name of debut year in debut dataframe
    filter_debuts_players_data = filter_debuts_players_data.rename({'Year': 'DYear'}, axis='columns')

    # Join Player debut data  with before debut/debut statistics
    before_debut_dataset = pd.concat([filter_debuts_players_data, complete_milb_players_stats], axis=1)
    before_debut_dataset = getSinglesStat(before_debut_dataset)

    debut_dataset = pd.concat([filter_debuts_players_data, complete_mlb_players_stats], axis=1)
    debut_dataset = getSinglesStat(debut_dataset)

    # Export data to .csv
    before_debut_dataset[players_stats_output_columns].to_csv(before_debut_stats_file)
    debut_dataset[players_stats_output_columns].to_csv(debut_stats_file)
