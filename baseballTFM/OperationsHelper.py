import pandas as pd

def mean_stats_calculator(dataframe, by, stats):
    """
    Calculate mean for every stat
    :param dataframe: dataframe with needed data
    :param by: stats used as denominator. Stat to divide for
    :param stats: Stats to be averaged
    :return: DataFrame
    """
    for stat in stats:
        dataframe[stat] = dataframe[stat] / dataframe[by]
    return dataframe


def getSinglesStat(dataframe):
    """
    Calculate the "singles" (S) of a player
    :param dataframe: dataframe with needed data
    :return: DataFrame
    """
    dataframe['S'] = dataframe['H'].astype(int)- (dataframe['2B'].astype(int) + dataframe['3B'].astype(int) + dataframe['HR'].astype(int))
    return dataframe

def get_combination_value(league_combinations):
    """
    Get ASCII value for every league
    :param league_combinations: leagues to get ASCII values
    :return: int
    """
    sum = 0
    for league_string in league_combinations:
        for letter in league_string.strip():
            sum += ord(letter)
    return sum


